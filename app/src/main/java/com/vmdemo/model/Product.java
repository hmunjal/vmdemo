package com.vmdemo.model;

/**
 * Created by himanshu on 09-01-2020.
 */

public class Product {
    String pName;
    int price;
    String shortDescription;
    int id;
    int quantity;

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }



    public String getpName() {
        return pName;
    }

    public int getPrice() {
        return price;
    }

    public String getShortDescription() {
        return shortDescription;
    }
}
