package com.vmdemo.core;

import android.util.ArrayMap;
import android.util.Log;

import com.vmdemo.model.Product;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by himanshu on 09-01-2020.
 */

public class UserCart {
    private static UserCart ucart;
    ArrayMap<Integer,Product> productArrayMap;
    int cartSubtotal;
    static boolean  isCouponApplied = false;
    private  UserCart(){
        productArrayMap = new ArrayMap<>();
    }

    public static UserCart getInstance(){
        if(ucart == null){
            ucart = new UserCart();
        }
        isCouponApplied = false;

        return ucart;
    }

    public Product getProductAtIndex(int pos){
        return  productArrayMap.valueAt(pos);
    }
    public void addProductTocart(int key,Product p){
        productArrayMap.put(key, p);
    }
    public boolean applyCoupon(String coupon){
        boolean flag = false;
        if(coupon.equalsIgnoreCase("2020")){
            isCouponApplied =  true;
            return true;
        }else
            return false;
    }
    public boolean removecoupon(){
        isCouponApplied = false;
        return isCouponApplied;
    }

    public void inCreaseproductquantity(int key){

        Product p = productArrayMap.get(key);
        if(p != null){
            p.setQuantity(p.getQuantity()+1);
            productArrayMap.replace(key,p);
        }

        //Log.i("updated","cart"+productArrayMap);
    }
    public int getQtyatPos(int key){

        return productArrayMap.get(key).getQuantity();
    }
    public void deCreaseProductquantitiy(int key){

        Product p = productArrayMap.get(key);
        if(p != null){
            if(p.getQuantity() == 1){
                productArrayMap.remove(key);
                return;
            }
            p.setQuantity(p.getQuantity()- 1);

            productArrayMap.replace(key,p);
            Log.i("updated","cart"+productArrayMap.values());
        }


    }
    public int getTotalCount(){
        return productArrayMap.size();
    }
    public  int getTotalprice(){
        int price = 0;
        for (int i = 0; i < productArrayMap.size(); i++) {
            Product p = productArrayMap.valueAt(i);
            price = price+(p.getPrice() * p.getQuantity());
        }
        cartSubtotal = price;
        return  price;
    }
    public void setTotalprice(int price){
        cartSubtotal = price;
    }
    public int getCarttaxes(){
        return 10;
    }
    public int getGrandTotal(){
        return  getTotalprice() + getCarttaxes();
    }
    public boolean isCouponApplied(){
        return isCouponApplied;
    }
    public int getdiscountedGrandTotal(){
        int price = getGrandTotal();
        int discount = (price*20)/100;
        return  price - discount;
    }

}
