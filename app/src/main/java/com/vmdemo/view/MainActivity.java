package com.vmdemo.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.vmdemo.R;
import com.vmdemo.core.UserCart;
import com.vmdemo.model.Product;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements productAdapter.OnAddListner{
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    CardView bottombar;
    TextView cartNo,cartTotal;
    UserCart cart;
    ArrayList<Product> productList;
    String pound = "\u00a3";

    @Override
    public void showBottomBar(boolean showBar) {
        if(showBar){
            bottombar.setVisibility(View.VISIBLE);
            setBottomBarValues();
        }else
            bottombar.setVisibility(View.GONE);

    }

    void setBottomBarValues(){
        if(cart.getTotalCount() > 1)
            cartNo.setText(cart.getTotalCount()+" Items");
        else
            cartNo.setText(cart.getTotalCount()+" Item");
        cartTotal.setText(pound+cart.getTotalprice());
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.product_recycler_view);
        bottombar = (CardView) findViewById(R.id.bottombar);
        cartNo = (TextView) bottombar.findViewById(R.id.cart_no);
        cartTotal = (TextView) bottombar.findViewById(R.id.cart_total);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        productList = new ArrayList<>();
        addProducts();
        cart = UserCart.getInstance();
        // specify an adapter (see also next example)
        mAdapter = new productAdapter(productList,this,this);
        recyclerView.setAdapter(mAdapter);
        bottombar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent i = new Intent(MainActivity.this,CartSummary.class);
                    startActivity(i);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(cart.getTotalCount() == 0) {
            showBottomBar(false);
            mAdapter.notifyDataSetChanged();
        }

    }

    void addProducts(){
        for (int i =1;i<5;i++){
            Product p = new Product();
            p.setId(i);
            p.setpName("Product "+i);
            p.setPrice(10*i);
            p.setShortDescription("Product description goes here");
            productList.add(p);
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
