package com.vmdemo.view;

import android.app.Application;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vmdemo.R;
import com.vmdemo.core.UserCart;
import com.vmdemo.model.Product;

import java.util.ArrayList;

/**
 * Created by himanshu on 09-01-2020.
 */

public class productAdapter extends RecyclerView.Adapter<productAdapter.MyViewHolder>{
    private ArrayList<Product> productList;
    Context context;
    OnAddListner addClicked;
    UserCart cart;
    public interface OnAddListner {
        public void showBottomBar(boolean showbar);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView pNameTv,priceTV,desctv,qtyTV;
        CardView addBtn;
        LinearLayout btnWrapper;
        RelativeLayout minusBtn,PlusBtn;
        public MyViewHolder(View v) {
            super(v);
            pNameTv = (TextView) v.findViewById(R.id.p_name);
            priceTV = (TextView) v.findViewById(R.id.p_price);
            desctv = (TextView) v.findViewById(R.id.p_desc);
            addBtn = (CardView) v.findViewById(R.id.add_btn);
            btnWrapper = (LinearLayout) v.findViewById(R.id.btn_wrapper);
            minusBtn = (RelativeLayout) v.findViewById(R.id.minus_btn);
            PlusBtn = (RelativeLayout) v.findViewById(R.id.plus_btn);
            qtyTV = (TextView) v.findViewById(R.id.qty_tv);
        }
    }

    public void setOnAddlistner(OnAddListner listner){
        addClicked = listner;
    }

    public productAdapter(ArrayList myDataset, Context con,OnAddListner listner) {
        productList = myDataset;
        Log.i("list","size"+productList.size());
        context = con;
        addClicked = listner;
        cart = UserCart.getInstance();

    }


    @Override
    public productAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        String pound = "\u00a3";
        final Product p = productList.get(position);
        holder.pNameTv.setText(p.getpName());
        holder.desctv.setText(p.getShortDescription());
        holder.priceTV.setText(pound+p.getPrice());
        p.setQuantity(1);

        holder.addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Product added to Cart.",Toast.LENGTH_SHORT).show();
                holder.addBtn.setVisibility(View.GONE);
                holder.btnWrapper.setVisibility(View.VISIBLE);

                cart.addProductTocart(p.getId(),p);
                addClicked.showBottomBar(true);
            }
        });
        holder.PlusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cart.inCreaseproductquantity(p.getId());
                holder.qtyTV.setText(""+cart.getQtyatPos(p.getId()));
                addClicked.showBottomBar(true);
            }
        });
        holder.minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cart.getQtyatPos(p.getId()) == 1){
                    cart.deCreaseProductquantitiy(p.getId());
                    holder.btnWrapper.setVisibility(View.GONE);
                    holder.addBtn.setVisibility(View.VISIBLE);
                }else{
                    cart.deCreaseProductquantitiy(p.getId());
                    holder.qtyTV.setText(""+cart.getQtyatPos(p.getId()));
                }
                    if(cart.getTotalCount() == 0)
                        addClicked.showBottomBar(false);
                else
                    addClicked.showBottomBar(true);
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return productList.size();
    }
}
