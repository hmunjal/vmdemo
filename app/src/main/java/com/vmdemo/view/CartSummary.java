package com.vmdemo.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vmdemo.R;
import com.vmdemo.core.UserCart;
import com.vmdemo.model.Product;

import org.w3c.dom.Text;

public class CartSummary extends AppCompatActivity {
    UserCart cart;
    LinearLayout cartProductsLL;
    EditText couponEt;
    CardView applyBtn;
    TextView subTotalTv,taxesTv,GrandtotalTV,discountTv,apply_btn_tv;
    RelativeLayout discountWrapper;
    LinearLayout screenWrapper;
    TextView noItemTv;
    String pound = "\u00a3";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_summary);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        cart = UserCart.getInstance();
        screenWrapper = (LinearLayout) findViewById(R.id.screen_wrapper);
        noItemTv = (TextView) findViewById(R.id.no_item_tv);
        cartProductsLL = (LinearLayout) findViewById(R.id.cart_products);
        couponEt = (EditText) findViewById(R.id.coupon_et);
        applyBtn = (CardView) findViewById(R.id.apply_btn);
        subTotalTv = (TextView) findViewById(R.id.cart_subtotal);
        taxesTv = (TextView)  findViewById(R.id.cart_taxes);
        GrandtotalTV = (TextView) findViewById(R.id.cart_grandtotal);
        discountWrapper = (RelativeLayout) findViewById(R.id.discountWrapper);
        discountTv = (TextView) findViewById(R.id.cart_discount);
        apply_btn_tv = (TextView) findViewById(R.id.apply_btn_tv);
        addCartProductUI(false);
        setcartPrices();

        applyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeybaord(couponEt);
                String coupon = couponEt.getText().toString().trim();
                if(cart.isCouponApplied()){
                    cart.removecoupon();
                    setcartPrices();
                    couponEt.setText(null);
                    discountWrapper.setVisibility(View.GONE);
                    apply_btn_tv.setText("APPLY");
                }else{
                    if(coupon !=null && coupon.length() > 1){
                        if(cart.applyCoupon(coupon)){
                            setDiscountedcartPrice();
                            Toast.makeText(CartSummary.this,"Coupon applied successfully",Toast.LENGTH_SHORT).show();
                            discountWrapper.setVisibility(View.VISIBLE);
                            discountTv.setText(""+(cart.getGrandTotal() - cart.getdiscountedGrandTotal()));
                            apply_btn_tv.setText("REMOVE");
                        }else
                            Toast.makeText(CartSummary.this,"Enter a valid coupon",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(CartSummary.this,"Enter a valid coupon",Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }
    void setDiscountedcartPrice(){
        subTotalTv.setText(""+cart.getTotalprice());
        taxesTv.setText(""+cart.getCarttaxes());
        GrandtotalTV.setText(""+cart.getdiscountedGrandTotal());

    }
    void setcartPrices(){
        subTotalTv.setText(""+cart.getTotalprice());
        taxesTv.setText(""+cart.getCarttaxes());
        GrandtotalTV.setText(""+cart.getGrandTotal());
    }
    private void hideKeybaord(View v) {
        InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getApplicationWindowToken(),0);
    }
    void addCartProductUI(boolean isUpdate){
        if(isUpdate)
            cartProductsLL.removeAllViews();;
        for (int i = 0; i < cart.getTotalCount(); i++) {
            View v = LayoutInflater.from(this)
                    .inflate(R.layout.product_item, cartProductsLL, false);
            TextView pNameTv = (TextView) v.findViewById(R.id.p_name);
            TextView priceTV = (TextView) v.findViewById(R.id.p_price);
            TextView desctv = (TextView) v.findViewById(R.id.p_desc);
            CardView addBtn = (CardView) v.findViewById(R.id.add_btn);
            LinearLayout btnWrapper = (LinearLayout) v.findViewById(R.id.btn_wrapper);
            RelativeLayout minusBtn = (RelativeLayout) v.findViewById(R.id.minus_btn);
            RelativeLayout PlusBtn = (RelativeLayout) v.findViewById(R.id.plus_btn);
            TextView qtyTV = (TextView) v.findViewById(R.id.qty_tv);

            desctv.setVisibility(View.GONE);
            addBtn.setVisibility(View.GONE);
            btnWrapper.setVisibility(View.VISIBLE);

            final Product p = cart.getProductAtIndex(i);
            pNameTv.setText(p.getpName());
            priceTV.setText(pound+p.getPrice());
            qtyTV.setText(""+p.getQuantity());

            PlusBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cart.inCreaseproductquantity(p.getId());
                    addCartProductUI(true);
                    setcartPrices();
                }
            });
            minusBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cart.deCreaseProductquantitiy(p.getId());
                    addCartProductUI(true);
                    setcartPrices();
                    if(cart.getTotalCount() == 0){
                        screenWrapper.setVisibility(View.GONE);
                        noItemTv.setVisibility(View.VISIBLE);
                    }
                }
            });

            cartProductsLL.addView(v);
        }
    }

}
